#include "Ghost.h"

Ghost::Ghost()
{
	// Test change
	this->SetCoords();
	this->RollDirection();
}
/*---------------------
IN:
 - N/A
OOUT:
 Void: Changes the Direction of the Entity.
---------------------*/
void Ghost::RollDirection()
{
	this->setDirection(Roll4Sides());
}

Direction Ghost::getDirection()
{
	return this->GDirection;
}

EntityType Ghost::getType()
{
    return EntityType::ET_Ghost;
}

void Ghost::setDirection(Direction direction)
{
	this->GDirection = direction;
}

Coords Ghost::Update(Error statusError)
{
    if (statusError == Error::E_BLOCKEDWALL)
    {
        this->RollDirection();
    }
    else if (statusError == Error::E_BLOCKEDPACMAN)
    {
        this->RollDirection();
    }
    else if (CoinFlip())
    {
        this->RollDirection();
    }
    Coords Old = this->getCoords();
    Coords current = this->getCoords();
    if (this->GDirection == Direction::UP && Old.y > 0)
    {
        current.y--;
    }
    else if (this->GDirection == Direction::DOWN && Old.y < 20)
    {
        current.y++;
    }
    else if (this->GDirection == Direction::LEFT && Old.x > 0)
    {
        current.x--;
    }
    else if (this->GDirection == Direction::RIGHT && Old.x < 20)
    {
        current.x++;
    }
    else
    {
        this->RollDirection();
    }
    return current;
}
