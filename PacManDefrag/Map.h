#pragma once
#include <string>
#include "Entity.h"
class Map
{
    
    static std::vector<Entity*> entityList;
    void DoCoordinates(); 
    int IsOverAnotherEntity(Coords coordinate);
    unsigned int modecool = 0;
	unsigned int eaten = 0;
public:
    static int Points;
    bool gameover = false;
	bool gamewon = false;
	char MainMap[21][21];
	Map();
	~Map();
	void MapInitalise(std::string name);
	char InitalMap[21][21];
    void MapUpdate();
    bool spawnEntity(Entity* entity);
    bool IsBlocked(Coords coordinate);
    unsigned int countEntity();
	unsigned int countEntityBerry();
    unsigned int getEats();
	void clearEntities();
    bool ThotMode = false;
};