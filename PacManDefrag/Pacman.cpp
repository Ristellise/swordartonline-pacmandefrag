#include "Pacman.h"
#include <Windows.h>
Coords Pacman::Update(Error statusError)
{
    Coords proposed = this->getCoords();
  
    if (statusError == Error::E_BLOCKEDWALL)
    {
        return proposed;
    }
    if ((GetKeyState(VK_UP) & 0x8000) && proposed.y != 0)
    {
        proposed.y -= 2;
        proposed.direction = Direction::UP;
        return proposed;
    }
    if ((GetKeyState(VK_DOWN) & 0x8000) && proposed.y != 20)
    {
        proposed.y += 2;
        proposed.direction = Direction::DOWN;
        return proposed;
    }
    if ((GetKeyState(VK_LEFT) & 0x8000) && proposed.x != 0)
    {
        proposed.x -= 2;
        proposed.direction = Direction::LEFT;
        return proposed;
    }
    if ((GetKeyState(VK_RIGHT) & 0x8000) && proposed.x != 20)
    {
        proposed.x += 2;
        proposed.direction = Direction::RIGHT;
        return proposed;
    }
    return proposed; // Catch all issues.
}

EntityType Pacman::getType()
{
    return EntityType::ET_PCMan;
}

Pacman::Pacman()
{
}

Pacman::~Pacman()
{
}
