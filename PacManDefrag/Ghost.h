#pragma once
#include "Entity.h"

class Ghost : public Entity
{
public:
	Ghost();
	void RollDirection();
	~Ghost();
    Coords Update(Error statusError);
	void setDirection(Direction direction);
	Direction getDirection();
    EntityType getType();
private:
	Direction GDirection;
};

