#include "Utils.h"


std::string PathCurrent()
{
	char cCurrentPath[FILENAME_MAX];
	bool success = GetCurrentDir(cCurrentPath, sizeof(cCurrentPath));
	if (success)
	{
		return std::string(cCurrentPath);
	}
	else
	{
		return std::string("Can't find current path...\n");
	}
}
/*--------------------------------
- Generate 1 position coordinate.
IN:
 - N/A
OUT:
 - Coords: Coordinates in X and Y from 0 to 20
--------------------------------*/
Coords RandomiseStartPos()
{
	Coords coord;
	int z = rand();
	std::random_device rd;
	std::mt19937 generator(rd());
	std::uniform_int_distribution<int> unif(0,20);
	coord.x = unif(generator);
	coord.y = unif(generator);
	return coord;
}

/*--------------------------------
- Randomises the Direction.
IN:
 - N/A
OUT:
 - Direction: Enum Direction.
--------------------------------*/
Direction Roll4Sides()
{
	Coords coord;
	int z;
	std::random_device rd;
	std::mt19937 generator(rd());
	std::uniform_int_distribution<int> unif(0, 3);
	z = unif(generator);
	return (Direction)z;
}

/*--------------------------------
- Flips a coin.
IN:
 - N/A
OUT:
 - bool: Value either True or False.
--------------------------------*/
bool CoinFlip()
{
	int z = rand();
	std::random_device RandDevice;
	std::mt19937 Generator(RandDevice());
	std::uniform_int_distribution<int> TrueFalse(0,1);
	return (bool)TrueFalse(Generator);
}

bool operator== (const Coords& op1, const Coords& op2)
{
    if (op1.x == op2.x && op1.y == op2.y)
    {
        return true;
    }
    else
    {
        return false;
    }
}