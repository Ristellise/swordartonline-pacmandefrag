#include "Berry.h"



Berry::Berry()
{
	this->SetCoords(RandomiseStartPos());
}

Berry::~Berry()
{
}

Coords Berry::Update(Error statusError)
{
    return this->getCoords();
}

EntityType Berry::getType()
{
    return EntityType::ET_BERRY;
}
