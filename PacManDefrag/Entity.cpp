#include "Entity.h"
#include <cstdio>
int Entity::Entitycount = 0;

Entity::Entity()
{
	this->pos.x = 0;
	this->pos.y = 0;
	Entity::Entitycount++;
}

Coords Entity::Update(Error statusError)
{
	printf("Undefined Entity Update called. If you see this, you messed up!");
    return pos;
}

EntityType Entity::getType()
{
    return EntityType::ET_NULL;
}

Entity::Entity(int x, int y)
{
	this->pos.x = x;
	this->pos.y = y;
	Entity::Entitycount++;
}

void Entity::SetCoords()
{
	this->pos = RandomiseStartPos();
}

void Entity::SetCoords(Coords coord)
{
	this->pos = coord;
}

void Entity::SetCoords(int x, int y)
{
	this->pos.x = x;
	this->pos.y = y;
}

Entity::~Entity() 
{
	Entity::Entitycount--;
}

Coords Entity::getCoords()
{
	return this->pos;
}
