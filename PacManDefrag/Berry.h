#pragma once
#include "Entity.h"
class Berry :
	public Entity
{
public:
	Berry();
	~Berry();
    Coords Update(Error statusError);
    EntityType getType();
};