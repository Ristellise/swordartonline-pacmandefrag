#pragma once
#ifndef UTIL
#define UTIL

#include <string>
#include <stdio.h>  /* defines FILENAME_MAX */

// Windows Test
#ifdef _WIN32
#include <direct.h>
#define GetCurrentDir _getcwd
#else
#include <unistd.h>
#define GetCurrentDir getcwd
#endif

#include <random>

enum Direction
{
	UP,
	DOWN,
	LEFT,
	RIGHT,
    D_NULL,
	COUNT
};

enum Error
{
    E_NULL,
    E_BLOCKEDWALL,
    E_BLOCKEDPACMAN
};

struct Coords
{
	int x, y;
    bool ignoreflags = false;
    Direction direction = D_NULL;
};

bool operator== (const Coords& op1, const Coords& op2);

Coords RandomiseStartPos();
bool CoinFlip();
Direction Roll4Sides();
std::string PathCurrent();
#endif