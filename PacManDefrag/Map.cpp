#include "Map.h"
#include <fstream>
#include <cstdio>
#include "Utils.h"
std::vector<Entity*> Map::entityList = {};
int Map::Points = 0;
Map::Map()
{
	for (size_t i = 0; i < 21; i++)
	{
		for (size_t z = 0; z < 21; z++)
		{
			MainMap[i][z] = ' ';
		}
	}
    gameover = false;
    Points = 0;
}

bool Map::spawnEntity(Entity* entity)
{
	if (!Map::IsBlocked(entity->getCoords()))
	{
		Map::entityList.push_back(entity);
		return true;
	}
	else
	{
		return false;
	}
}
bool isDeadCheck(Entity* ent)
{
    if (ent == nullptr)
    {
        return true;
    }
    if (ent == NULL)
    {
        return true;
    }
    if (ent->isDead)
    {
        return true;
    }
    else
    {
        return false;
    }
}

void Map::clearEntities()
{
	this->entityList.clear();
}

void Map::DoCoordinates()
{
    // Refresh map
    for (size_t i = 0; i < 21; i++)
    {
        for (size_t z = 0; z < 21; z++)
        {
            MainMap[i][z] = ' ';
        }
    }
    // recursively remove files that are null. freeing up vector.
    Map::entityList.erase(std::remove_if(Map::entityList.begin(), Map::entityList.end(), isDeadCheck),
        Map::entityList.end());

    // Time to draw entities!
    for (size_t i = 0; i < Map::entityList.size(); i++)
    {
        if (Map::entityList[i] != NULL)
        {
            Coords c = Map::entityList[i]->getCoords();
            EntityType ET = Map::entityList[i]->getType();
            if (ET == EntityType::ET_Ghost)
            {
                MainMap[c.y][c.x] = '\xE3';
            }
            else if (ET == EntityType::ET_PCMan && !this->ThotMode)
            {
                MainMap[c.y][c.x] = 'P';
            }
			else if (ET == EntityType::ET_PCMan && this->ThotMode)
			{
				MainMap[c.y][c.x] = '\xE4';
			}
            else
            {
                MainMap[c.y][c.x] = '?';
            }
        }
        
    }
}

void Map::MapUpdate()
{
    for (size_t i = 0; i < Map::entityList.size(); i++)
    {
        if (Map::entityList[i] != NULL)
        {
            Coords proposal = Map::entityList[i]->Update(Error::E_NULL);
            Coords proposalbackby1 = Map::entityList[i]->getCoords();
            // Check proposed coordinates.
            // Entity is a PCman
            int entid = -1;
            if (Map::entityList[i]->getType() == ET_PCMan)
            {
                switch (proposal.direction)
                {
                case Direction::UP:
                    proposalbackby1.y--;
                    break;
                case Direction::DOWN:
                    proposalbackby1.y++;
                    break;
                case Direction::LEFT:
                    proposalbackby1.x--;
                    break;
                case Direction::RIGHT:
                    proposalbackby1.x++;
                    break;
                default:
                    break;
                }
				if (this->modecool > 0)
				{
					this->modecool--;
				}
				else
				{
					this->ThotMode = false;
				}

				if (!this->IsBlocked(proposalbackby1))
				{
					entid = this->IsOverAnotherEntity(proposalbackby1);
					// Prevent suicde R key.
					if (entid != -1 && !this->ThotMode && Map::entityList[entid]->getType() == ET_Ghost)
					{
						delete Map::entityList[i]; // Destroy Entity
						Map::entityList[i] = nullptr;
						gameover = true;
						break;
					}
					else if (entid != -1 && this->ThotMode && Map::entityList[entid]->getType() == ET_Ghost)
					{
						delete Map::entityList[entid];
						Map::entityList[entid] = nullptr;
						this->Points+=15;
						this->eaten++;
					}
					else if (entid != -1 && Map::entityList[entid]->getType() == ET_BERRY)
					{
						delete Map::entityList[entid];
						Map::entityList[entid] = nullptr;
						this->Points += 5;
						this->ThotMode = true;
						this->modecool = 5;
					}
					Map::entityList[i]->SetCoords(proposalbackby1);
					if (!this->IsBlocked(proposal))
					{
						entid = this->IsOverAnotherEntity(proposal);
						if (entid != -1 && !this->ThotMode && Map::entityList[entid]->getType() == ET_Ghost)
						{
							delete Map::entityList[i]; // Destroy Entity
							Map::entityList[i] = nullptr;
							gameover = true;
							break;
						}
						else if (entid != -1 && this->ThotMode && Map::entityList[entid]->getType() == ET_Ghost)
						{
							delete Map::entityList[entid];
							Map::entityList[entid] = nullptr;
							this->Points += 15;
							this->eaten++;
						}
						else if (entid != -1 && Map::entityList[entid]->getType() == ET_BERRY)
						{
							delete Map::entityList[entid];
							Map::entityList[entid] = nullptr;
							this->Points += 5;
							this->ThotMode = true;
							this->modecool = 5;
						}
						Map::entityList[i]->SetCoords(proposal);
					}
				}
				else
				{
					// Do nothing
				}
            }
            // Entity is a Ghost
            else if (Map::entityList[i]->getType() == ET_Ghost)
            {
                // not blocked by wall
                if (!this->IsBlocked(proposal))
                {
                    entid = this->IsOverAnotherEntity(proposal);
                    if (entid != -1 && !this->ThotMode && Map::entityList[entid]->getType() == ET_PCMan)
                    {
                        delete Map::entityList[i]; // Destroy Entity
                        Map::entityList[i] = nullptr;
                        gameover = true;
                    }
                    else if (entid != -1 && this->ThotMode && Map::entityList[entid]->getType() == ET_PCMan)
                    {
                        delete Map::entityList[entid];
                        Map::entityList[entid] = nullptr;
                        this->Points += 5;
                    }
					else if (entid != -1 && Map::entityList[entid]->getType() == ET_Ghost)
					{
						unsigned int cc = 0;
						while (true)
						{
							if (cc > 2)
							{
								break;
							}
							else
							{
								proposal = Map::entityList[i]->Update(Error::E_BLOCKEDPACMAN);
								if (!this->IsBlocked(proposal))
								{
									entid = this->IsOverAnotherEntity(proposal);
									Map::entityList[i]->SetCoords(proposal);
									if (entid != -1 && !this->ThotMode && Map::entityList[entid]->getType() == ET_PCMan)
									{
										delete Map::entityList[i]; // Destroy Entity
										Map::entityList[i] = nullptr;
										gameover = true;
										break;
									}
									else if (entid != -1 && this->ThotMode && Map::entityList[entid]->getType() == ET_PCMan)
									{
										delete Map::entityList[entid];
										Map::entityList[entid] = nullptr;
										this->Points += 15;
										this->eaten++;
										break;
									}
									else
									{
										// Remeber to break to prevent random teleportation!
										Map::entityList[i]->SetCoords(proposal);
										break;
									}

								}
								else
								{
									cc++;
								}

							}
						}
					}
                    else
                    {
                        Map::entityList[i]->SetCoords(proposal);
                    }
                }
                // Direction going is facing a wall.
                else
                {
                    unsigned int cc = 0;
                    while (true)
                    {
                        if (cc > 2)
                        {
                            break;
                        }
                        else
                        {
                            proposal = Map::entityList[i]->Update(Error::E_BLOCKEDWALL);
                            if (!this->IsBlocked(proposal))
                            {
                                entid = this->IsOverAnotherEntity(proposal);
                                Map::entityList[i]->SetCoords(proposal);
                                if (entid != -1 && !this->ThotMode && Map::entityList[entid]->getType() == ET_PCMan)
                                {
                                    delete Map::entityList[i]; // Destroy Entity
                                    Map::entityList[i] = nullptr;
                                    gameover = true;
                                    break;
                                }
                                else if (entid != -1 && this->ThotMode && Map::entityList[entid]->getType() == ET_PCMan)
                                {
                                    delete Map::entityList[entid];
                                    Map::entityList[entid] = nullptr;
                                    this->Points += 15;
									this->eaten++;
                                    break;
                                }
								else
								{
									// Remeber to break to prevent random teleportation!
									Map::entityList[i]->SetCoords(proposal);
									break;
								}
                                
                            }
                            else
                            {
                                cc++;
                            }
                            
                        }
                    }
                }
            }
        }
    }
    Map::DoCoordinates();
}

int Map::IsOverAnotherEntity(Coords coordinate)
{
    for (size_t i = 0; i < Map::entityList.size(); i++)
    {
        // Tell the entity we are updating
        // and then get the coordinates of it.
        if (Map::entityList[i] != NULL)
        {
            Coords eCoords = Map::entityList[i]->getCoords();
            if (eCoords == coordinate)
            {
                return i;
            }
        }
        
    }
    return -1;
}

unsigned int Map::countEntityBerry()
{
	unsigned int c = 0;
	for (size_t i = 0; i < Map::entityList.size(); i++)
	{
		if (i != NULL && Map::entityList[i]->getType() == ET_BERRY)
		{
			c++;
		}
	}
	return c;
}

unsigned int Map::getEats()
{
	return this->eaten;
}

unsigned int Map::countEntity()
{
    unsigned int c = 0;
    for (size_t i = 0; i < Map::entityList.size(); i++)
    {
        if (i != NULL && Map::entityList[i]->getType() != ET_BERRY)
        {
            c++;
        }
    }
    return c;
}

// This checks the inital map if it is blocked or not.
// does NOT check for entity collision.
bool Map::IsBlocked(Coords coordinate)
{
    if (coordinate.x > 20 || coordinate.y > 20 ||
        coordinate.x < 0  || coordinate.y < 0)
    {
        return true;
    }
    else if (InitalMap[coordinate.y][coordinate.x] != ' ')
    {
        return true;
    }
    else
    {
        return false;
    }
}

void Map::MapInitalise(std::string name)
{
	std::ifstream file(name.c_str());
	unsigned int x = 0;
	if (!file.is_open())
	{
		printf(PathCurrent().c_str());
		printf("\n");
		printf("Map is invalid! Can't open file. Loading Blank slate...\n");
		for (int x = 0; x < 21; x++)
		{
			for (size_t i = 0; i < 21; i++)
			{
				InitalMap[x][i] = ' ';
			}
		}
	}
	else
	{
        std::string str;
		while (std::getline(file, str))
		{
			for (size_t i = 0; i < str.size(); i++)
			{
                if (str[i] == 'X')
                {
                    InitalMap[x][i] = 'X';
                }
                else
                {
                    InitalMap[x][i] = ' ';
                }
				
			}
			x++;
		}
	}
}

Map::~Map()
{
}
