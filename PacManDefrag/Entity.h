#pragma once
#include "Utils.h"
enum EntityType
{
    ET_NULL,
    ET_PCMan,
    ET_Ghost,
    ET_BERRY,
    ET_Count
};

class Entity
{
public:
    bool isDead = false;
	Entity();
	Entity(int x, int y);
	~Entity();
	Coords getCoords();
	static int Entitycount;
	// Set Coordinates
	void SetCoords();
	void SetCoords(Coords coord);
	void SetCoords(int x, int y);
	virtual Coords Update(Error statusError);
    virtual EntityType getType();
private:
	Coords pos;
};