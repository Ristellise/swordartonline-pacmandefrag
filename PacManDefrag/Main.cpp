#include <iostream>
#include <cstdio>
#include <conio.h>
#include <chrono>
#include <thread>
#include <Windows.h>
#include "Map.h"
#include "Pacman.h"
#include "Ghost.h"
#include "Berry.h"

std::chrono::system_clock::time_point tnow = std::chrono::system_clock::now();
std::chrono::system_clock::time_point tlater = std::chrono::system_clock::now();

void ClearScreen()
{
	HANDLE                     hStdOut;
	CONSOLE_SCREEN_BUFFER_INFO csbi;
	DWORD                      count;
	DWORD                      cellCount;
	COORD                      homeCoords = { 0, 0 };

	hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
	if (hStdOut == INVALID_HANDLE_VALUE) return;

	/* Get the number of cells in the current buffer */
	if (!GetConsoleScreenBufferInfo(hStdOut, &csbi)) return;
	cellCount = csbi.dwSize.X *csbi.dwSize.Y;

	/* Fill the entire buffer with spaces */
	if (!FillConsoleOutputCharacter(
		hStdOut,
		(TCHAR) ' ',
		cellCount,
		homeCoords,
		&count
	)) return;

	/* Fill the entire buffer with the current colors and attributes */
	if (!FillConsoleOutputAttribute(
		hStdOut,
		csbi.wAttributes,
		cellCount,
		homeCoords,
		&count
	)) return;

	/* Move the cursor home */
	SetConsoleCursorPosition(hStdOut, homeCoords);
}

bool getKeyPress()
{
	if (GetKeyState(VK_UP) & 0x8000)
	{
		return true;
	}
	if (GetKeyState(VK_DOWN) & 0x8000)
	{
		return true;
	}
	if (GetKeyState(VK_LEFT) & 0x8000)
	{
		return true;
	}
	if (GetKeyState(VK_RIGHT) & 0x8000)
	{
		return true;
	}
	if (GetKeyState('R') & 0x8000)
	{
		return true;
	}
	else
	{
		return false;
	}
}

void ShowConsoleCursor(bool showFlag)
{
	HANDLE out = GetStdHandle(STD_OUTPUT_HANDLE);

	CONSOLE_CURSOR_INFO     cursorInfo;

	GetConsoleCursorInfo(out, &cursorInfo);
	cursorInfo.bVisible = showFlag; // set the cursor visibility
	SetConsoleCursorInfo(out, &cursorInfo);
}

void gotoxy(int x, int y) {
	COORD pos = { x, y };
	HANDLE output = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleCursorPosition(output, pos);
}

void draw(char map[21][21],bool ignoreSpaces = false)
{
	gotoxy(0, 1);
    printf("--------------------");
	for (size_t i = 0; i < 21; i++)
	{
        for (size_t i2 = 0; i2 < 21; i2++)
        {
            if (ignoreSpaces && map[i][i2] == ' ')
            {
                // pass
            }
            else
            {
                gotoxy(i2, 3 + i);
                if (map[i][i2] == 'X')
                {
                    std::cout << '\xDB';
                }
                else
                {
                    std::cout << map[i][i2];
                }
                
            }
            
        }
        gotoxy(0, 3+i);
	}
}

void doreset(Map MapInstance)
{
	for (size_t i = 0; i < 3; i++)
	{
		while (true)
		{
			if (MapInstance.spawnEntity(new Ghost()))
			{
				break;
			}
		}
	}

	for (size_t i = 0; i < 2; i++)
	{
		while (true)
		{
			if (MapInstance.spawnEntity(new Berry()))
			{
				break;
			}
		}
	}
}

int main()
{
	unsigned long long int framec = 0;
	unsigned long long int stepc = 0;
    unsigned int Maxentities = 5;
	unsigned int MaxBerry = 3;
	bool debug = true;
	bool init = true;
    Pacman* pacmaninstance = new Pacman();
	pacmaninstance->SetCoords(1, 1);
	Map MapInstance;
	MapInstance.MapInitalise("Map.txt");
	draw(MapInstance.InitalMap);
    MapInstance.spawnEntity(pacmaninstance);
	ShowConsoleCursor(false);
	doreset(MapInstance);
	while (true)
	{
		tnow = std::chrono::system_clock::now();
		std::chrono::duration<double, std::milli> work_time = tnow - tlater;

		if (work_time.count() < 60)
		{
			std::chrono::duration<double, std::milli> delta_ms(60 - work_time.count());
			auto delta_ms_duration = std::chrono::duration_cast<std::chrono::milliseconds>(delta_ms);
			std::this_thread::sleep_for(std::chrono::milliseconds(delta_ms_duration.count()));
			gotoxy(0, 25);
			if (debug)
			{
				// Debugging frame stepping.
				//printf("Delay: %.2f | Frame: %lld | Step: %lld", 1.0 / delta_ms_duration.count(), framec, stepc);
				framec++;
			}
		}

		tlater = std::chrono::system_clock::now();
		std::chrono::duration<double, std::milli> sleep_time = tlater - tnow;
		// Render first. Then update
		gotoxy(0, 22);
		if (getKeyPress())
		{
			//PacInstance.Update();
			// Render Code
			//ClearScreen();
			if (!MapInstance.gameover)
			{
				MapInstance.MapUpdate(); // Update events
				draw(MapInstance.InitalMap); // Draw inital map
				//ClearScreen();
				draw(MapInstance.MainMap, true); // Do overlay
				stepc++;
				if (MapInstance.countEntity() < Maxentities && stepc % 3 == 0)
				{
					MapInstance.spawnEntity(new Ghost());
				}
				gotoxy(0, 26);
				printf("Points: %i | Ghosts Eaten: %i | Ghosts On Field: %i", MapInstance.Points, MapInstance.getEats(),MapInstance.countEntity());
			}
			if (MapInstance.countEntityBerry() == 0)
			{
				MapInstance.gamewon = true;
			}
			if (MapInstance.countEntity() == 0)
			{
				MapInstance.gamewon = true;
			}
			if (GetKeyState('R') && (MapInstance.gameover || MapInstance.gamewon))
			{
				MapInstance.gameover = false;
				MapInstance.gamewon = false;
				MapInstance.Points = 0;
				// Flush all the entities.
				MapInstance.clearEntities();
				pacmaninstance = new Pacman();
				pacmaninstance->SetCoords(1, 1);
				MapInstance.spawnEntity(pacmaninstance);
				gotoxy(0, 27);
				printf("                                 ");
				stepc = 0;
				doreset(MapInstance);
				
				ClearScreen();
				draw(MapInstance.InitalMap); // Draw inital map
			}
			if (MapInstance.gameover)
			{
				gotoxy(0, 27);
				printf("You died! Press \"R\" to retry...");
			}
			if (MapInstance.gamewon)
			{
				gotoxy(0, 27);
				printf("You Won! Press \"R\" to retry...");
			}
			
		}

		
		
		
	}
	return 100;
}